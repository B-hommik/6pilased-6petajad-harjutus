﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6pilased_6petajad_harjutus
{
    class Student : People
        {
        public static List<Student> StudentList = new List<Student>();
        public string Form;//klass
        public Student(string id) : base(id) {
            StudentList.Add(this);
        } //see konstruktor on baasklassis, seega on vaja sealt siia üle tuua
        public override string ToString() => $"{Form} klassi õpilane {FirstName} {LastName}";
    }
}
