﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6pilased_6petajad_harjutus
{
    class People
    {
        public static Dictionary<string, People> School = new Dictionary<string, People>();
        public static List<People> SchoolList = new List<People>();
        private string ID;
        public string FirstName;
        public string LastName;
  /*      public string FullName => $"{FirstName} {LastName}";*///KONSTRUKTOR, KUI MUUTA NIME SIIS SEE FULLNAMES EI KAJASTU. kui tahaksime et need muutuksid, siis tuleks teha hoopis property GET/SET asjaga
        public string FullName //teeme selle propertyks, siis muutub Fullname ka siis, kui me muudame eesnime või perenime. Kuna set'i pole, siison tegemist readonly propertyga
        {
            get => FirstName + " " + LastName;
        }
        //saad kirjutada ka => FirstName + "" +"LastName";
        //public DateTime DateofBirth => new DateTime(year(), int.Parse[]ID.Substring(3, 2)), int.Parse
        //public string ID
        //{ get => id;
        //  set => id = id == "" ? value : id;//juhul kui id on "", siis on ta value ja kui ta ei ole tühi, siis ta on id
        //}
         public People(string id) //kõikidel autodel peab olema numbrimärk
        {
            ID = id;// Numbrimärk on väli ja numbrivärk on parameeter. Kui sul on need sõnad erinevalt kirjutatud, siis saad this eest ära jätta, sest ta saab ise aru mis on mis. ''This'' on kirjutatud helelillana, mitte tumesinisena, järelikult optional.

            if (!School.ContainsKey(id)) School.Add(id, this);//Esimene lause ütleb kui EI OLE seda numbrit, teine lause, ALLES SIIS lisame dictionarysse. Sama numbrimärgi vältimine, kuna dictionarys ei tohi olla kaks korda sama võtmega asja- saad vea
        }

        //funktsioon mis teisendab IK DateTimeks. Kui tahan ka teises klassis seda kasutada, siis pigem tuleks teha see stringiks. Kui tahad ainult siin klassis kasutada, siis tuleb instants.
        public DateTime BirthDay()
        {
            int sajand = 1800;
            if (ID[0] > '2') sajand = 1900;
            if (ID[0] > '4') sajand = 2000;
            return new DateTime(
                sajand + //siia liidetakse otsa kas 1800, 1900 või 2000 vastavalt eelnevale if tsüklile, mis otsib mis numbriga isikukood algab
                int.Parse(ID.Substring(1,2)), //SIIA ARVUTAME AASTA, AGA SAAME AINULT VIIMASED NUMBRID, SEEGA TULEB KA LEIDA ESIMESED NUMBRID IF TSÜKLIGA ENNE
                int.Parse(ID.Substring(3,2)), //SIIA ARVUTAME KUU
                int.Parse(ID.Substring(5,2))); //SIIA ARVUTAME PÄEVAD
        }

        //dATETIME - DATETIME =TIMESPAN (AJAVAHEMIK)
        public int Age //=> (DateTime.Today - Synniaeg).Days*4/1461;
        {
            get //get funktsioon teeb sellest property
            { return (DateTime.Today - BirthDay()).Days * 4 / 1461; }
        }

    
        public override string ToString() //eriline string, ehk override.. muidu kui välja trükkida programmis, siis ta ütleb meil oma tüübi ja nime ainult.
        {
            return $"{FullName} ({ID})";
        }
    }
}
