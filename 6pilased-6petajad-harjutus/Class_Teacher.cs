﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6pilased_6petajad_harjutus
{
    class Teacher : People
    {
        public string Subject;
        public string Form;//klass
        public static List<Teacher> TeachertList = new List<Teacher>();
        public Teacher(string id) : base(id) {
            TeachertList.Add(this);
        }//see konstruktor on baasklassis, seega on vaja sealt siia üle tuua
        //public override string ToString() => $"{Subject} õpetaja {FirstName} {LastName} ({Form} klassi klassijuhataja)";
        public override string ToString() => $"Õppeaine {Subject} õpetaja {FirstName} {LastName}{(Form == "" ? "" : $"({Form} klassi juhataja)")}";
    }
}
