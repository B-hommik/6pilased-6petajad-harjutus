﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6pilased_6petajad_harjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            string failinimi = @"..\..\Teachers.txt";
            string[] failisisu = File.ReadAllLines(failinimi); //loeb ridade kaupa
            foreach (var failirida in failisisu)
            {
                var reaosad = (failirida + ",,,").Split(',');//Neid komasid on selleks vaja, et kindlalt saaks neli osa
                                                             //if (reaosad.Length == 4) Sellega kindlustab selle et tühje ridu ei lisata
                new Teacher(reaosad[0].Trim())//TRIM korjab ära tühikud koma ees ja koma taga, see null näitab väärtust mida kõige ees näidatakse.
                {
                    FirstName = reaosad[1].Trim(),
                    LastName = reaosad[2].Trim(),
                    Subject = reaosad[3].Trim(),
                    Form = reaosad[4].Trim(),

                };

            }
            failinimi = @"..\..\Students.txt";
            failisisu = File.ReadAllLines(failinimi); //loeb ridade kaupa
            foreach (var failirida2 in failisisu)
            {
                var reaosad2 = (failirida2 + ",,,").Split(',');//Neid komasid on selleks vaja, et kindlalt saaks neli osa
                                                               //if (reaosad.Length == 4) Sellega kindlustab selle et tühje ridu ei lisata
                new Student(reaosad2[0].Trim())//TRIM korjab ära tühikud koma ees ja koma taga, , see null näitab väärtust mida kõige ees näidatakse.
                {
                    FirstName = reaosad2[1].Trim(),
                    LastName = reaosad2[2].Trim(),
                    Form = reaosad2[3].Trim(),

                };
            }
            Console.WriteLine("Kooli Dictionary: ");

            foreach (var x in People.School) // trükib välja terve dictionary sisu
            {
                Console.WriteLine(x);
            }
            Console.WriteLine("Õpilaste nimekiri: ");
            foreach (var y in Student.StudentList) // trükib välja terve dictionary sisu
            {
                Console.WriteLine(y);
            }
            Console.WriteLine("Õpetajate nimekiri: ");
            foreach (var z in Teacher.TeachertList) // trükib välja terve dictionary sisu
            {
                Console.WriteLine(z);
            }
            foreach (var a in People.School.Values)
            {
                Console.WriteLine(a.BirthDay());//Birthday.ToShortDateString, siis ei tule kellaaega. Või .ToString("dd. MMMM YYYY aastal")
            }

            //KELLEL ON JÄRGMISENA SÜNNIPÄEV
            //järgmisel sünnipäeval saab inimene aasta vanemaks
            //järgmise sünnipäeva kuupäev on Sünnikuupäev. AddYears(vanus +1)

            DateTime next = DateTime.Today.AddDays(400); //järgmine sünnipäev tuleb aasta jooksul, 400 päeva on antud lihtsalt varuga. Võib ka kirjutada DateTime next = DateTime.axValue ehk kõikide aegade lõpp
            string kes = ""; //siia salvestatakse isikukoodid
            foreach (var x in People.School)
                if (x.Value.BirthDay().AddYears(x.Value.Age + 1) < next) //võtame kellegi sealt nimekirjast ja kui tal on sünnipäev enne 400 päeva, siis jätame ta meelde ja nii käime kõik läbi kuni jääbki ainult see kellel on kõige varem.
                {
                    kes = x.Key;
                    next = x.Value.BirthDay().AddYears(x.Value.Age + 1);
                }
            Console.WriteLine($"Järgmine sünnipäevalaps on {People.School[kes].FullName}"); //kui paneks lihtsalt kes siis prindiks välja Isikukoodi

            DateTime week = DateTime.Today.AddDays(7); //järgmine sünnipäev tuleb aasta jooksul, 400 päeva on antud lihtsalt varuga. Võib ka kirjutada DateTime next = DateTime.axValue ehk kõikide aegade lõpp
            string kes2 = ""; //siia salvestatakse isikukoodid
            foreach (var x in People.School)
                if (x.Value.BirthDay().AddYears(x.Value.Age + 1) < next) //võtame kellegi sealt nimekirjast ja kui tal on sünnipäev enne 400 päeva, siis jätame ta meelde ja nii käime kõik läbi kuni jääbki ainult see kellel on kõige varem.
                {
                    kes = x.Key;
                    next = x.Value.BirthDay().AddYears(x.Value.Age + 1);
                }
            try //PROOVI TEHA SEDA ALUMIST, kui ei saa teha siis seda ei viida täide
            {
                Console.WriteLine($"Algaval nädalal on sünnipäevalaps {People.School[kes2].FullName}");
            }

            catch (Exception e) //KUI MIDAGI JUHTUB, SIIS TEE SEDA ALUMIST, ERANDOLUKORD.''e'' võid panna siis, kui tahad veateadet saada.
            {
                Console.WriteLine("See nädal kahjuks sünnipäevalapsi pole");
                Console.WriteLine(e.Message); //saad teada mis valesti läks
                                              //throw; kui tahad et sulle öeldakse ka viga. Kui ei taha veateadet, siis seda ei pea panema

            }
        }
    }
}
